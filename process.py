import matplotlib.pyplot as plt
import numpy as np
import os

from skimage import util
from skimage.color import rgb2hed
from skimage import io, color, filters
from skimage.filters import threshold_otsu
from skimage import data, exposure, img_as_float
from matplotlib.colors import LinearSegmentedColormap

def listar(de):
    #Variable para la ruta al directorio
    path = de
 
    #Lista vacia para incluir los ficheros
    name = []
 
    #Lista con todos los ficheros del directorio:
    lstDir = os.walk(path)   #os.walk()Lista directorios y ficheros
    #Crea una lista de los ficheros png que existen en el directorio y los incluye a la lista.
    i=0
    for root, dirs, files in lstDir:
        for fichero in files:
            (nombreFichero, extension) = os.path.splitext(fichero)
            if(extension == ".png"):
                name.append(nombreFichero)
        i += 1
        if i >= 1:
            break
    return name
	
def linealizar():
	path='40X'
	name = listar(path)

	for i in range(len(name)):
		img = io.imread(path+"/"+name[i]+'.png')
		histo=exposure.histogram(img[:, :, 0])

		a=histo[1].min()
		a1=histo[1].max()

		histo=exposure.histogram(img[:, :, 1])

		b=histo[1].min()
		b1=histo[1].max()

		histo=exposure.histogram(img[:, :, 2])

		c=histo[1].min()
		c1=histo[1].max()

		if(path=='10X'):
			img[:, :, 0]= exposure.rescale_intensity(img[:, :, 0], in_range=(a1-(1*a), a1)) # 10X=1 / 20X=0.4 / 40x=0.7
			img[:, :, 1]= exposure.rescale_intensity(img[:, :, 1], in_range=(b1-(2*b), b1))   # 10X=2 / 20X=1   / 40x=1.2
			img[:, :, 2]= exposure.rescale_intensity(img[:, :, 2], in_range=(c1-(4.5*c), c1))   # 10X=4.5 / 20X=12   / 40x=4
    
		if(path=='20X'):
			img[:, :, 0]= exposure.rescale_intensity(img[:, :, 0], in_range=(a1-(0.4*a), a1)) # 10X=1 / 20X=0.4 / 40x=0.7
			img[:, :, 1]= exposure.rescale_intensity(img[:, :, 1], in_range=(b1-(1*b), b1))   # 10X=2 / 20X=1   / 40x=1.2
			img[:, :, 2]= exposure.rescale_intensity(img[:, :, 2], in_range=(c1-(12*c), c1))   # 10X=4.5 / 20X=12   / 40x=4

		if(path=='40X'):
			img[:, :, 0]= exposure.rescale_intensity(img[:, :, 0], in_range=(a1-(0.7*a), a1)) # 10X=1 / 20X=0.4 / 40x=0.7
			img[:, :, 1]= exposure.rescale_intensity(img[:, :, 1], in_range=(b1-(1.2*b), b1))   # 10X=2 / 20X=1   / 40x=1.2
			img[:, :, 2]= exposure.rescale_intensity(img[:, :, 2], in_range=(c1-(4*c), c1))   # 10X=4.5 / 20X=12   / 40x=4

		io.imshow(img)
		io.show()

		io.imsave(path+'/linealizacion/'+name[i]+'_linealize_hist.png',(img)) #guardar la imagen con linealizado
	
def normalizar():
	path = '20X/linealizacion'
	name = listar(path)
	for i in range(len(name)):
		img = io.imread(path+"/"+name[i]+'.png')
		dat = np.array(img)+.0
		sol = np.empty_like(img) #matriz de ceros

		#formula de Expansión de contraste (255*(imagen-min))/(max-min)

		sol = np.divide(np.multiply(255,np.subtract(img,img.min(),dtype=float),dtype=float),np.subtract(img.max(),img.min(),dtype=float),dtype=float)

		sol = np.uint8(sol) #de Numpy a uint8
		io.imshow(sol)
		io.show()

		io.imsave("C://Users/Daniers/Documents/Daniers96-analisis-morfometrico-histologia-peces-hibridos-94e936a54bf8/20X/normalizacion/"+name[i]+'_norma.png',(sol))
	
def hematoxilina():
	path = '40X/normalizacion'
	name = listar(path)
	for i in range(len(name)):
		ihc_rgb = io.imread(path+"/"+name[i]+'.png')
		ihc_hed = rgb2hed(ihc_rgb)

		io.imshow(ihc_hed[:, :, 0])
		io.show()
		ihc_hed_np = np.array(ihc_hed)

		th = threshold_otsu(ihc_hed_np[:,:,0])
		sol = ihc_hed_np[:,:,0] <= th

		sol = np.array(sol*255)

		print(sol)

		io.imsave("C://Users/Daniers/Documents/Daniers96-analisis-morfometrico-histologia-peces-hibridos-94e936a54bf8/20X/binario/"+name[i]+'_bin.png',(np.uint8(sol)))

class MiClase:
    def __init__(self):
        print("una clase")